let panierListe = []
let totalPanierHT = 0
let nbProduits = 0
let totalPanierTTC = 0
const  tva = 0.2
 class Panier {
      ttc() {
        return  totalPanierTTC
      }
        ht() {
          return  totalPanierHT
        }
          ajoute(produit,qte) {
                let ht = produit.prix
                    panierListe.push(produit.nom)
                    nbProduits += qte
                    totalPanierHT += (ht * qte)
                    totalPanierTTC += (ht * qte) + (ht * tva)
                return `${produit.nom} ajouté au panier coutant ${ht} le total du panier HT = ${totalPanierHT} le total du panier TTC =${totalPanierTTC}`
              }
              retire(produit) {
                    let produitKey = panierListe.indexOf(produit.nom)
                    
                    delete panierListe[produitKey] // operateur delete array prend en parametre arr[key]
                    totalPanierHT - produit.prix
                    totalPanierTTC - (produit.prix + (produit.prix * tva))

                    // operateur delete array prend en parametre arr[key]

                 //   console.log(typeof produitKey);
                    return `${produit.nom} à été supprimé du panier`
                  }
          reset(){
             panierListe = []
             return 'le panier à été réinitialisé'
          }
        }; // fin Panier
  class Produit {
    constructor(nom,prix){
        this.nom = nom
        this.prix = prix
    }
}
  class Viennoiserie extends Produit {
    constructor(nom,prix,frais) {
      super(nom,prix)
      this.frais = frais
    }
  }
const monPanier = new Panier()
const baguette = new Produit('baguette',0.85)
const croissant = new Viennoiserie('croissant', 0.90,true)
const biscuit = new Produit('biscuit', 1.20)
const chocolatine = new Viennoiserie('chocolatine',0.95,true)
console.log(monPanier.ttc())
console.log(monPanier.ht())
console.log(monPanier.ajoute(baguette, 2))
monPanier.ajoute(croissant, 1)
monPanier.ajoute(chocolatine, 1)
monPanier.ajoute(biscuit, 2)
console.log(totalPanierHT)
console.log(totalPanierTTC)
console.log(panierListe)
console.log(monPanier.retire(croissant));
console.log(panierListe)
console.log(monPanier.reset())
console.log(panierListe)
console.log(totalPanierHT)
console.log(totalPanierTTC)
console.log(nbProduits)
console.log(baguette)
console.log(croissant)
